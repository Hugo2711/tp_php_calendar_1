<?php
session_start();

if ( ! isset( $_SESSION['login_datetime'] ) )
$_SESSION['login_datetime'] = date( 'c' );

if ( ! isset( $_COOKIE['login_datetime'] ) )
	setcookie( 'login_datetime', date( 'c' ), time()+60*60*24*30 ); // 30 jours
	
	echo 'Première connexion : ' . $_COOKIE['login_datetime'];
	echo '<br/>';
	echo 'Première connexion de cette session : ' . $_SESSION['login_datetime'];

	// Mois courant passé par paramètre
	if ( isset( $_REQUEST['month'] ) )
	{
		$current_month = (int)$_REQUEST['month'];
	}
	// Mois enregistré en cookie 
	elseif ( isset( $_COOKIE['current_month'] ) )
	{
		$current_month = (int)$_COOKIE['current_month'];
	}
	else
	{
		$current_month = date( 'n' );
	}
	
	// Année courante passé par paramètre
	if ( isset( $_REQUEST['year'] ) )
	{
		$current_year = (int)$_REQUEST['year'];
	}
// Annnée enregistrée en cookie 
elseif ( isset( $_COOKIE['current_year'] ) )
{
	$current_year = (int)$_COOKIE['current_year'];
}
else
{
	$current_year = date( 'Y' );
}
setcookie( 'current_month', $current_month, time()+60*60*24*30 ); // 30j en secondes
setcookie( 'current_year', $current_year, time()+60*60*24*30 ); // 30j en secondes

?>	
<html lang="en" class="">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<link rel="stylesheet" href="Calendar.css">
	<style class="cp-pen-styles" type="text/css">
	</style>

	<title>Calendar</title>
</head>

<body>
	<div class="wrapp">
		<div class="flex-calendar">
			
			<?php
				// Mois/année en cours			
				$this_month = strtotime( $current_year . '-' . $current_month );
				
				// Mois précédent - méthode 2
				$previous_month = date( 'm', strtotime( 'previous month', $this_month ) );
				$previous_year = date( 'Y', strtotime( 'previous month', $this_month ) );
				
				// Mois suivant - méthode 2
				$next_month = date( 'm', strtotime( 'next month', $this_month ) );
				$next_year = date( 'Y', strtotime( 'next month', $this_month ) );
				
				?>
			
			<div class="month">
				<a href="calendar.php?year=<?php echo $previous_year ?>&month=<?php echo $previous_month ?>" class="arrow visible"></a>

				<div class="label">
					<?php echo date( 'F Y', $this_month ); ?>
				</div>

				<a href="calendar.php?year=<?php echo $next_year ?>&month=<?php echo $next_month ?>" class="arrow visible"></a>
			</div>

			<div class="week">
				<div class="day">M</div>
				<div class="day">T</div>
				<div class="day">W</div>
				<div class="day">T</div>
				<div class="day">F</div>
				<div class="day">S</div>
				<div class="day">S</div>
			</div>

			<div class="days">
				
			<?php
				
				// Bornes du mois courant
				$first_day_of_month = date( 'N', strtotime( 'first day of ' . $current_year . '-' . $current_month ) );
				$last_day_of_month = date( 'd', strtotime( 'last day of ' . $current_year . '-' . $current_month ) );
				
				$today = date( 'Y-n-d' );
				$disabled = array();
				$events = array();

				
				// Transmission donnée formulaire	
				
				if ( isset( $_REQUEST['title'])  && $_REQUEST['title'] )
				
				{
					$title = isset( $_REQUEST['title'] ) ? $_REQUEST['title'] : '';
					
					$event_table = array( 'title' => $title );

					// sauvegarde session + cookie
					if(isset($_SESSION['events'])){
						foreach($_SESSION['events']	as $savedSessionEvent)
						$event_saved[]=$savedSessionEvent;
					}
					if(isset($_COOKIE['events'])){
						foreach($_COOKIE['events']as $savedCookieEvent);
						$event_saved=$savedCookieEvent;
					}
			
					
					// ajout image + déplacement
					if ( isset( $_FILES['image'] ) && $_FILES['image']['size'] )
					{				
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						if (finfo_file($finfo, $_FILES['image']['tmp_name'])=='image/jpeg')	
						{
							move_uploaded_file($_FILES['image']['tmp_name'], 'files/assets/'.$_FILES['image']['name'] . '.jpg');						
							
							
							$event_table['image'] = 'files/assets/'.$_FILES['image']['name'];
						}
					}
					
					$events[$_REQUEST['date']][] = $event_table;				
					// Enregistrement
					$_SESSION['events']= $events;				
			}
							
				// Décalage premier jour du mois
				for ( $i = 1; $i < $first_day_of_month; $i++ )
				{
					echo '<div class="day out"><div class="number"></div></div>';
				}

		
				// Calendrier
				for( $i = 1; $i <= $last_day_of_month; $i++ )
				{
					$event_info='';
					$classes = 'day';
					$current_day = new DateTime( $current_year . '-' . $current_month . '-' . $i );
					$new_current_day = $current_day->format('Y-m-d');

					// Si un jour est un événement
					if ( isset( $events[$new_current_day] ) ){
						$classes .= ' event';
						$name_event = '';
						foreach ( $events[$new_current_day]  as $event )
						$name_event .= $event['title'] .'<br />';
						
						// associe la classe "info" à la variable $event_info
						$event_info= '<div class="details">' . $name_event .'<img src="files/assets/' .$_FILES['image']['name'].'"width="100"/></div>';
					}
					
					if ( ( $current_year . '-' . $current_month . '-' . $i ) == $today ) $classes .= ' selected';
					
					if ( in_array( $i, $disabled ) ) $classes .= ' disabled';

					// Affichage de si c'est un événement ou un jour normal
					echo '<div class="' . $classes . '"><div class="number">' . $i . '</div>' . $event_info . '</div>';	

				}							
				?>
			</div>
		</div>	
		<form class="wrapp" method="post" enctype="multipart/form-data">
		<h2>Ajouter un événement</h2>
		<p>
			<label for="date">Date</label>
			<input type="date" name="date" id="date" value="<?php echo date( 'Y-m-d' ) ?>" required />
		</p>
		<p>
			<label for="title">Titre</label>
			<input type="text" name="title" id="title" value="" required/>
		</p>
		<p>
			<label for="image">Image</label>
			<input type="file" name="image" id="image" />
		</p>
		<button type="submit">Valider</button>
	</form>
	<div class="wrapp" id="table_event">
		<h2>Tableau des événements</h2>
		<ul>
			<?php
			// tableau des évents

				if ($events)
				{
					foreach ( $events as $date => $day_as_event)
					{
						foreach ( $day_as_event as $event )
						{
							$current_date = new DateTime( $date );
							$new_current_day=$current_date->format( 'd.m.Y' );

							echo '<p>' . $new_current_day. '<p> - ' . $event['title'];
							
							if ( isset( $event['image'] ) )
							echo '<br/><img src="files/assets/' .$_FILES['image']['name'].'"width="100" />';
								
						}
					}
				}
			?>
		</ul>
	</div>
	</div>
	</div>
</body>
</html>